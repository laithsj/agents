<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_hotels', function (Blueprint $table) {
            $table->engine = 'MyIsam';
            $table->bigIncrements('id');
            $table->unsignedInteger('trip_id')->unsigned()->index();;
            $table->foreign('trip_id')->references('id')->on('trips');
            $table->string('hotel_id',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_hotels');
    }
}
