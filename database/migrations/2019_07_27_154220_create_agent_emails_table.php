<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_emails', function (Blueprint $table) {
            $table->engine = 'MyIsam';
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('email_id')->nullable();
            $table->string('email',255)->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('email_id')->references('id')->on('email_typs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_emails');
    }
}
