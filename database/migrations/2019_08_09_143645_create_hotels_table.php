<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->engine = 'MyIsam';
            $table->bigIncrements('id');
            $table->string('hotel_id',255)->nullable();
            $table->string('name',255)->nullable();
            $table->string('country',10)->nullable();
            $table->string('country_id',255)->nullable();
            $table->string('hotel_type_id',255)->nullable();
            $table->string('city_id',255)->nullable();
            $table->string('city',255)->nullable();
            $table->string('address',255)->nullable();
            $table->string('exact_class',255)->nullable();
            $table->string('class',255)->nullable();
            $table->string('url',255)->nullable();
            $table->string('deep_link_url',255)->nullable();
            $table->string('currency',10)->nullable();
            $table->string('ranking',200)->nullable();
            $table->string('is_closed',5)->nullable();
            $table->string('zip',100)->nullable();
            $table->string('latitude',255)->nullable();
            $table->string('longitude',255)->nullable();
            $table->string('number_of_rooms',255)->nullable();
            $table->string('preferred',30)->nullable();
            $table->string('default_language',30)->nullable();
            $table->string('review_score',30)->nullable();
            $table->string('number_of_reviews',30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
