<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status',
    ];

    public function emails(){
        return $this->hasMany('App\AgentEmails');
    }

    public function phone(){
        return $this->hasMany('App\AgentMobiles');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static function checkIfAgentExist($email){
        return self::where('email',$email)->get();
    }

    public function addNewAgent($request,$password){
        $this->name     =   $request->name;
        $this->email    =   $request->email;
        $this->password =   $password;
        $this->save();
        return $this;
    }
}
