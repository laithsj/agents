<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    public $timestamp = true;

    public function country(){
		return $this->belongsTo('App\Country');
    }

    public function cities(){
		return $this->hasMany('App\TripCity');
    }

    public function hotels(){
		return $this->hasMany('App\TripHotel');
    }

    public function addTrip($from,$to,$age,$extra,$country,$agancyId,$pricePerPerson,$extraPrice,$totalPrice,$currency,$fromCountry = ' ',$toCountry = ' ',$tripType = 0){
        $this->trip_start_date = $from;
        $this->trip_end_date = $to;
        $this->country_id = $country;
        $this->trip_age = $age;
        $this->extra_details = $extra;
        $this->user_id = $agancyId;
        $this->trip_price_per_person = $pricePerPerson;
        $this->trip_extra_price = $extraPrice;
        $this->trip_total_price = $totalPrice;
        $this->currency = $currency;
        $this->from_country = $fromCountry;
        $this->to_country = $toCountry;
        $this->trip_type = (string)$tripType;
        $this->save();
        return $this;
    }

    public static function updateTrip($tripId,$from,$to,$age,$extra,$country,$agancyId,$pricePerPerson,$extraPrice,$totalPrice,$currency,$fromCountry = '',$toCountry = '',$tripType = 0){
        $tripId->trip_start_date = $from;
        $tripId->trip_end_date = $to;
        $tripId->country_id = $country;
        $tripId->trip_age = $age;
        $tripId->extra_details = $extra;
        $tripId->user_id = $agancyId;
        $tripId->trip_price_per_person = $pricePerPerson;
        $tripId->trip_extra_price = $extraPrice;
        $tripId->trip_total_price = $totalPrice;
        $tripId->currency = $currency;
        $tripId->from_country = $fromCountry;
        $tripId->to_country = $toCountry;
        $tripId->trip_type = $tripType;
        $tripId->save();
        return $tripId;
    }

    public static function getTrips($userId){
        return self::where('user_id',$userId)->orderBy('created_at','desc')->get();
    }

    public static function getTrip($tripId){
        return self::where('id',$tripId)->first();
    }

    public static function removeTrip($tripId){
        self::where('id',$tripId)->delete();
    }
}
