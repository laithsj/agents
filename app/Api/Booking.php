<?php

namespace App\Api;
use Cache;

class Booking{
    public function bookingAPI($case,$iso,$offset = 0){
        $token = 'eff3cf319b9144f991aaeb8a1e71d320';
        if($case == 'getHotelsByCountry'){
            $url = 'https://distribution-xml.booking.com/2.4/json/hotels?country_ids='.$iso.'&language=en&extras=hotel_info&rows=1000&offset='.$offset;
        }elseif($case == 'getBookingCites'){
            $url = 'https://distribution-xml.booking.com/2.4/json/cities?countries='.$iso.'&languages=en&rows=1000';
        }


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$token");
        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Connection: Keep-Alive',
                'X-Access-Token: eff3cf319b9144f991aaeb8a1e71d320',
                'Authorization: Basic '. base64_encode("laith.sj@happytripway.com:FaCEBOOk20")
            )
        );
        $result = curl_exec($ch);
        /*if(curl_exec($ch) === false) {
            echo 'Curl error: ' . curl_error($ch);
        } else {
            echo 'Operation completed without any errors /n';
        }*/
        curl_close($ch);

        return json_decode($result, true);
    }
}
