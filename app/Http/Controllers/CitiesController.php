<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cities;

class CitiesController extends Controller
{
    public function getCityByCountry(Request $request){
        $cities = Cities::getCountryCities($request->countryId);

        $result = 'error';
        if(count($cities) > 0){
            $result = $cities;
        }

        return view('trip.citiesList',['cities'  =>  $result]);
    }
}
