<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Cities;
use App\Trip;
use App\TripCity;
use Auth;
use Cache;
use App\Hotel;
use App\TripHotel;

class TripController extends Controller
{
    public function viewTripsList(Request $request){
        $message = '';
        if(isset($request->message) && trim($request->message) != ''){
            $message = $request->message;
        }

        // get user trips
        $trips = Trip::getTrips(Auth::user()->id);

        return view('trip.tripsList',['trips' => $trips,'message' => $message,'messageStatus' => $request->messageStatus]);
    }

    public function viewCreateInternalTrip(Request $request){
        // get countries list
        $countries = Country::getCountriesList();

        $message = '';
        if(isset($request->message) && trim($request->message) != ''){
            $message = $request->message;
        }

        // redirect user to create trip view
        return view('trip.createTripInternal',['countries' => $countries,'message' => $message, 'messageStatus' => $request->messageStatus]);
    }

    public function createTripInternal(Request $request){
        $trip = $request->all();

        // trip Country
        $tripCountry = $trip['countrySelected'];

        // trip date
        $tripDateRange = explode("-",$trip['date']);
        $dateFrom = date_format(date_create($tripDateRange[0]),'Y-m-d H:i:s');
        $dateTo = date_format(date_create($tripDateRange[1]),'Y-m-d H:i:s');

        // age restriction
        $age = $trip['age'];

        // extra details
        $extraData = $trip['content'];

        // trip price
        $pricePerPerson = $trip['tripPrice'];
        $extraPrice = $trip['extraPrice'];
        $totalPrice = $pricePerPerson + $extraPrice;
        $currency = $trip['currency'];

        // add trip
        $tripModel = new Trip;
        $addTrip = $tripModel->addTrip($dateFrom,$dateTo,$age,$extraData,$tripCountry,Auth::user()->id,$pricePerPerson,$extraPrice,$totalPrice,$currency);

        $result = false;
        if(!empty($addTrip)){
            if(isset($trip['file'])){
                // move file
                $destinationPath = '/home/laith/Desktop/trips/' . $addTrip->id . '_trip';
                $this->createDirectory($destinationPath);
                $filesNames = array();
                foreach ($trip['file'] as $file) {
                    $name = time() . str_random(20) . '.' . $file->getClientOriginalExtension();
                    $filesNames[] = $name;
                    $file->move($destinationPath, $name);
                }
            }
            
            // add trip cities
            for ($i=0; $i < count($trip['selectedCity']); $i++) {
                $tripCities = new TripCity;
                $tripCities = $tripCities->addTripCity($addTrip->id,$trip['selectedCity'][$i]);
            }

            $result = true;
        }

        if($result){
            // redirec to list with success
            return redirect(action('TripController@viewTripsList',['messageStatus' => 'success','message' => '']));
        }else{
            // redirect to add trip with error
            return redirect(action('TripController@viewCreateInternalTrip',['messageStatus' => 'fail','message' => 'Something went wrong while adding the trip to the list! Please try again later or contact our customer support.']));
        }

    }

    public function createDirectory($dir, $permission = 0755){
        if (!file_exists($dir)) {
            return mkdir($dir, $permission, true);
        }
        return true;
    }

    public function viewTripDetails(Request $request){
        $tripId = $request->id;
        if(md5($tripId.Auth::user()->id) == $request->enc){
            $trip = Trip::getTrip($tripId);

            // if(count($trip) == 0){
            //     return redirect(action('TripController@viewTripsList',['messageStatus' => 'fail','message' => 'Something went wrong while viewing the selected trip']));
            // }

            $tripFilesDir = '/home/laith/Desktop/trips/' . $tripId . '_trip';
            $this->createDirectory('/home/laith/Desktop/trips/' . $tripId . '_trip/temp/');
            $tripFiles = '/home/laith/Desktop/trips/' . $tripId . '_trip/temp/'.$tripId . '_kyc.zip';

            // Get real path for our folder
            $rootPath = realpath($tripFilesDir);

            // Initialize archive object
            $zip = new \ZipArchive();
            $zip->open($tripFiles, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

            $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($rootPath),
                \RecursiveIteratorIterator::LEAVES_ONLY
            );

            foreach ($files as $name => $file){
                    // Skip directories (they would be added automatically)
                if (!$file->isDir()){
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);
                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }
            // Zip archive will be created only after closing object
            $zip->close();

            if($trip->country_id == 0){
                // get countries name
                $trip->countryFrom = Country::getCountryNameByIso($trip->from_country)->name;
                $trip->countryTo = Country::getCountryNameByIso($trip->to_country)->name;
            }

            return view('trip.viewTrip',['trip' => $trip]);
        }else{
            // return to list page
            return redirect(action('TripController@viewTripsList',['messageStatus' => 'fail','message' => 'Something went wrong while viewing the selected trip']));
        }
    }

    public function downloadTripFiles(Request $request){
        $tripId = $request->id;
        if(md5($tripId.Auth::user()->id) == $request->enc){
            $tripFiles = '/home/laith/Desktop/trips/' . $tripId . '_trip/temp/'.$tripId . '_kyc.zip';
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($tripFiles));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($tripFiles));
            readfile($tripFiles);
        }
    }

    public function viewEditTrip(Request $request){
        $tripId = $request->id;
        if(md5($tripId.Auth::user()->id) == $request->enc){
            // get countries list
            $countries = Country::getCountriesList();
            $trip = Trip::getTrip($tripId);

            // if(count($trip) == 0){
            //     return redirect(action('TripController@viewTripsList',['messageStatus' => 'fail','message' => 'Something went wrong while viewing the selected trip']));
            // }

            $view = 'trip.editInternalTrip';
            $extraName = 'cities';

            // get country city
            $extra = Cities::getCountryCities($trip->country_id);
            if($trip->trip_type == 1){
                $view = 'trip.editExternalTrip';
                $extraName = 'hotels';
                $extra = $trip->hotels;
            }

            return view($view,['trip' => $trip,'fileLocation' => '','messageStatus' => '','countries' => $countries,$extraName => $extra]);
        }else{
            // return to list page
            return redirect(action('TripController@viewTripsList',['messageStatus' => 'fail','message' => 'Something went wrong while viewing the selected trip']));
        }
    }

    public function editTrip(Request $request){
        $trip = $request->all();

        $tripId = $trip['tripId'];

        // trip Country
        $tripCountry = $trip['countrySelected'];
        $tripFromCountry = '';
        $tripToCountry = '';
        $tripType = 0;
        if(isset($trip['countrySelected2'])){
            $tripFromCountry = $trip['countrySelected'];
            $tripToCountry = $trip['countrySelected2'];
            $tripCountry = 0;
            $tripType = 1;
            // get countries name
            $trip['countryFrom'] = Country::getCountryNameByIso($trip['countrySelected'])->name;
            $trip['countryTo'] = Country::getCountryNameByIso($trip['countrySelected2'])->name;
        }

        // trip date
        $tripDateRange = explode("-",$trip['date']);
        $dateFrom = date_format(date_create($tripDateRange[0]),'Y-m-d H:i:s');
        $dateTo = date_format(date_create($tripDateRange[1]),'Y-m-d H:i:s');

        // age restriction
        $age = $trip['age'];

        // extra details
        $extraData = $trip['content'];

        // trip price
        $pricePerPerson = $trip['tripPrice'];
        $extraPrice = $trip['extraPrice'];
        $totalPrice = $pricePerPerson + $extraPrice;
        $currency = $trip['currency'];

        // update trip
        $tripRecord = Trip::getTrip($tripId);
        $editTrip = Trip::updateTrip($tripRecord,$dateFrom,$dateTo,$age,$extraData,$tripCountry,Auth::user()->id,$pricePerPerson,$extraPrice,$totalPrice,$currency,$tripFromCountry,$tripToCountry,$tripType);

        if(!isset($trip['countrySelected2'])){
            // remove all cities that is related to this trip and add new cities
            TripCity::removeTripCities($tripId);
            for ($i=0; $i < count($trip['selectedCity']); $i++) {
                $tripCities = new TripCity;
                $tripCities = $tripCities->addTripCity($tripId,$trip['selectedCity'][$i]);
            }
        }else{
            // remove all hotels that is related to this trip and add new hotels if edited
            TripHotel::removeTripHotels($tripId);
            for ($i=0; $i < count($trip['selectedHotels']); $i++) {
                $tripHotel = new TripHotel;
                $tripHotel = $tripHotel->addTripHotel($tripId,$trip['selectedHotels'][$i]);
            }
        }

        // check if the user uploaded new files
        if(isset($trip['file']) && count($trip['file']) > 0){
            $tripFiles = '/home/laith/Desktop/trips/' . $tripId . '_trip';
            unset($tripFiles);

            // move file
            $destinationPath = '/home/laith/Desktop/trips/' . $tripId . '_trip';
            $this->createDirectory($destinationPath);
            $filesNames = array();
            foreach ($trip['file'] as $file) {
                $name = time() . str_random(20) . '.' . $file->getClientOriginalExtension();
                $filesNames[] = $name;
                $file->move($destinationPath, $name);
            }

        }

        if(isset($trip['countrySelected2'])){
            // get countries name
            $editTrip->countryFrom = Country::getCountryNameByIso($trip['countrySelected'])->name;
            $editTrip->countryTo = Country::getCountryNameByIso($trip['countrySelected2'])->name;
        }

        return view('trip.viewTrip',['trip' => $editTrip]);

    }

    public function deleteTrip(Request $request){
        if(md5($request->id.Auth::user()->id) == $request->enc){
            Trip::removeTrip($request->id);
            TripCity::removeTripCities($request->id);
            $tripFiles = '/home/laith/Desktop/trips/' . $request->id . '_trip';
            unset($tripFiles);
        }

        // get user trips
        $trips = Trip::getTrips(Auth::user()->id);
        return view('trip.tripsList',['trips' => $trips,'message' => '','messageStatus' => '']);
    }

    public function viewTripExternal(Request $request){

        // get countries list
        $countries = Country::getCountriesList();

        $message = '';
        if(isset($request->message) && trim($request->message) != ''){
            $message = $request->message;
        }

        // redirect user to create trip view
        return view('trip.createTripExternal',['countries' => $countries,'message' => $message, 'messageStatus' => $request->messageStatus]);
    }

    public function createTripExternal(Request $request){
        $trip = $request->all();

        // trip Country
        $tripCountry = $trip['countrySelected'];

        // trip date
        $tripDateRange = explode("-",$trip['date']);
        $dateFrom = date_format(date_create($tripDateRange[0]),'Y-m-d H:i:s');
        $dateTo = date_format(date_create($tripDateRange[1]),'Y-m-d H:i:s');

        // age restriction
        $age = $trip['age'];

        // extra details
        $extraData = $trip['content'];

        // trip price
        $pricePerPerson = $trip['tripPrice'];
        $extraPrice = $trip['extraPrice'];
        $totalPrice = $pricePerPerson + $extraPrice;
        $currency = $trip['currency'];

        // from country to country
        $fromCountry = $trip['countrySelected'];
        $toCountry = $trip['countrySelected2'];

        // add trip
        $tripModel = new Trip;
        $addTrip = $tripModel->addTrip($dateFrom,$dateTo,$age,$extraData,0,Auth::user()->id,$pricePerPerson,$extraPrice,$totalPrice,$currency,$fromCountry,$toCountry,1);

        $result = true;
        
        if(isset($trip['file'])){
            // move file
            $destinationPath = 'C:\xampp\htdocs\agents\trips' . $addTrip->id . '_trip';
            $this->createDirectory($destinationPath);
            $filesNames = array();
            foreach ($trip['file'] as $file) {
                $name = time() . str_random(20) . '.' . $file->getClientOriginalExtension();
                $filesNames[] = $name;
                $file->move($destinationPath, $name);
            }
        }
        

        // add trip cities
        for ($i=0; $i < count($trip['selectedHotels']); $i++) {
            $tripHotels = new TripHotel;
            $hotels = $tripHotels->addTripHotels($addTrip->id,$trip['selectedHotels'][$i]);
        }


        if($result){
            // redirec to list with success
            return redirect(action('TripController@viewTripsList',['messageStatus' => 'success','message' => '']));
        }else{
            // redirect to add trip with error
            return redirect(action('TripController@viewTripExternal',['messageStatus' => 'fail','message' => 'Something went wrong while adding the trip to the list! Please try again later or contact our customer support.']));
        }
    }
}
