<?php

namespace App\Http\Controllers;
use App\AgentEmails;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Hash;
class RegistrationController extends Controller
{
    public function registerView(){
        return view('registration');
    }

    public function register(Request $request){
        // hash password
        $isUserExist = User::checkIfAgentExist($request->email);
        if(count($isUserExist) > 0){
            return view('registration',['message' => 'Email already exist']);
        }else{
            $agent = new User;
            $password = Hash::make(trim($request->password));
            $createdAt = $agent->addNewAgent($request,$password);

            // set file destination
            $destinationPath = "C:\\xampp\htdocs\agents\documents/" . $createdAt->id;
            $this->createDirectory($destinationPath);
            $filesNames = array();
            foreach ($request->myFile as $file) {
                $name = 'commercial.' . $file->getClientOriginalExtension();
                $filesNames[] = $name;
                $file->move($destinationPath, $name);
            }

            return view('thanks');
        }

        return view('registration');
    }


    public function createDirectory($dir, $permission = 0755){
        if (!file_exists($dir)) {
            return mkdir($dir, $permission, true);
        }
        return true;
    }
}
