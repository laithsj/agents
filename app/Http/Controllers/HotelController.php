<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api\Booking;
use Cache;
use App\Hotel;
class HotelController extends Controller
{
    public function getHotelsByCountry(Request $request){
        $iso = $request->countryId;

        $hotels = array();
        // check if there is hotesl exist for the giving country
        if(Cache::get('hotels_'.$iso)){
            // get hotels list from the cache
            $hotels = Cache::get('hotels_'.$iso);
        }else{
            $booking = new Booking;

            // check if hotel is exist in the database
            $hotels = Hotel::getHotelsByCountry($iso);
            if(count($hotels) == 0){
                $hotelsApi = $booking->bookingAPI('getHotelsByCountry',$iso);
                for ($i=0; $i < count($hotelsApi['result']); $i++) {
                    $hotelId = $hotelsApi['result'][$i]['hotel_id'];
                    // check if the id exist in cache
                    Cache::forget('hotel_'.$hotelId);
                    if(!Cache::has('hotel_'.$hotelId)){
                        // check if the hotel is exist in the database
                        $hotel = Hotel::getHotelById($hotelId);
                        if(count($hotel) == 0){
                            // add new hotel data
                            $hotelModel = new Hotel;
                            $hotelData = $this->validateRequest($hotelsApi['result'][$i]);
                            $addHotel = $hotelModel->addNewHotel(
                                $hotelData['hotel_id'],
                                $hotelData['default_language'],
                                $hotelData['address'],
                                $hotelData['hotel_type_id'],
                                $hotelData['url'],
                                $hotelData['city_id'],
                                $hotelData['review_score'],
                                $hotelData['deep_link_url'],
                                $hotelData['currency'],
                                $hotelData['exact_class'],
                                $hotelData['is_closed'],
                                $hotelData['ranking'],
                                $hotelData['number_of_reviews'],
                                $hotelData['zip'],
                                $hotelData['city'],
                                $hotelData['class'],
                                $hotelData['name'],
                                $hotelData['number_of_rooms'],
                                $hotelData['country'],
                                $hotelData['preferred'],
                                $hotelData['longitude'],
                                $hotelData['latitude']
                            );

                            Cache::put('hotel_'.$hotelsApi['result'][$i]['hotel_id'],$addHotel,30*30);
                        }else{
                            Cache::put('hotel_'.$hotel->hotel_id,$hotel,30*30);
                        }
                    }
                }

                $hotels = Hotel::getHotelsByCountry($iso);
            }
        }

        return $hotels;
    }

    public function validateRequest($hotelsApi){
        $hotelId = '';
        $default_language = '';
        $address = '';
        $hotel_type_id = '';
        $url = '';
        $city_id = '';
        $review_score = '';
        $deep_link_url = '';
        $currency = '';
        $exact_class = '';
        $is_closed = '';
        $ranking = '';
        $number_of_reviews = '';
        $zip = '';
        $city = '';
        $class = '';
        $name = '';
        $number_of_rooms = '';
        $country = '';
        $preferred = '';
        $longitude = '';
        $latitude = '';

        if(isset($hotelsApi['hotel_id'])){
            $hotelId = $hotelsApi['hotel_id'];
        }
        if(isset($hotelsApi['hotel_data']['default_language'])){
            $default_language = $hotelsApi['hotel_data']['default_language'];
        }
        if(isset($hotelsApi['hotel_data']['address'])){
            $address = $hotelsApi['hotel_data']['address'];
        }
        if(isset($hotelsApi['hotel_data']['hotel_type_id'])){
            $hotel_type_id = $hotelsApi['hotel_data']['hotel_type_id'];
        }
        if(isset($hotelsApi['hotel_data']['url'])){
            $url = $hotelsApi['hotel_data']['url'];
        }
        if(isset($hotelsApi['hotel_data']['url'])){
            $city_id = $hotelsApi['hotel_data']['city_id'];
        }
        if(isset($hotelsApi['hotel_data']['review_score'])){
            $review_score = $hotelsApi['hotel_data']['review_score'];
        }
        if(isset($hotelsApi['hotel_data']['deep_link_url'])){
            $deep_link_url = $hotelsApi['hotel_data']['deep_link_url'];
        }
        if(isset($hotelsApi['hotel_data']['currency'])){
            $currency = $hotelsApi['hotel_data']['currency'];
        }
        if(isset($hotelsApi['hotel_data']['exact_class'])){
            $exact_class = $hotelsApi['hotel_data']['exact_class'];
        }
        if(isset($hotelsApi['hotel_data']['is_closed'])){
            $is_closed = $hotelsApi['hotel_data']['is_closed'];
        }
        if(isset($hotelsApi['hotel_data']['ranking'])){
            $ranking = $hotelsApi['hotel_data']['ranking'];
        }
        if(isset($hotelsApi['hotel_data']['number_of_reviews'])){
            $number_of_reviews = $hotelsApi['hotel_data']['number_of_reviews'];
        }
        if(isset($hotelsApi['hotel_data']['zip'])){
            $zip = $hotelsApi['hotel_data']['zip'];
        }
        if(isset($hotelsApi['hotel_data']['city'])){
            $city = $hotelsApi['hotel_data']['city'];
        }
        if(isset($hotelsApi['hotel_data']['class'])){
            $class = $hotelsApi['hotel_data']['class'];
        }
        if(isset($hotelsApi['hotel_data']['name'])){
            $name = $hotelsApi['hotel_data']['name'];
        }
        if(isset($hotelsApi['hotel_data']['number_of_rooms'])){
            $number_of_rooms = $hotelsApi['hotel_data']['number_of_rooms'];
        }
        if(isset($hotelsApi['hotel_data']['country'])){
            $country = $hotelsApi['hotel_data']['country'];
        }
        if(isset($hotelsApi['hotel_data']['preferred'])){
            $preferred = $hotelsApi['hotel_data']['preferred'];
        }
        if(isset($hotelsApi['hotel_data']['location']['longitude'])){
            $longitude = $hotelsApi['hotel_data']['location']['longitude'];
        }
        if(isset($hotelsApi['hotel_data']['location']['latitude'])){
            $latitude = $hotelsApi['hotel_data']['location']['latitude'];
        }

        $hotelArray = [
            'hotel_id' => $hotelId ,
            'default_language' => $default_language ,
            'address' => $address ,
            'hotel_type_id' => $hotel_type_id ,
            'url' =>    $url ,
            'city_id' => $city_id,
            'review_score' =>    $review_score ,
            'deep_link_url' =>    $deep_link_url ,
            'currency' =>    $currency ,
            'exact_class' =>    $exact_class ,
            'is_closed' => $is_closed,
            'ranking' =>    $ranking ,
            'number_of_reviews' =>    $number_of_reviews ,
            'zip' =>    $zip ,
            'city' =>    $city ,
            'class' =>    $class ,
            'name' =>    $name ,
            'number_of_rooms' =>    $number_of_rooms ,
            'country' =>    $country ,
            'preferred' =>    $preferred ,
            'longitude' =>    $longitude ,
            'latitude' =>    $latitude ,
        ];

        return $hotelArray;
    }
}
