<?php

namespace App\Http\Controllers;
use App\AgentEmails;
use Illuminate\Http\Request;
use Auth;
class ProfileController extends Controller
{
    public function viewPorfile(){
        return view('profile.viewProfile',['agent' => \Auth::user()]);
    }

    public function viewEditProfile(){
        return view('profile.viewEditProfile',['agent' => \Auth::user(),'message' => '']);
    }

   
}
