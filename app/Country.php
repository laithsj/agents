<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;
class Country extends Model
{
    protected $table = 'countries';

    public static function getCountriesList(){
        // if(Cache::has('countries')){
        //     $countries = Cache::get('countries');
        // }else{
            $countries = self::get();
        //     Cache::put('countries',$countries,30*12);
        // }
        return $countries;
    }

    public static function getCountryNameByIso($iso){
        return self::select('name')->where('code',$iso)->first();
    }
}
