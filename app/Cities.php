<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Cities extends Model
{
    protected $table = 'cities';
    
    public static function getCountryCities($countryId){
        if(Cache::has('cities_'.$countryId)){
            $cities = Cache::get('cities_'.$countryId);
        }else{
            $cities = self::where('country_id',$countryId)->get();
            Cache::put('cities_'.$countryId,$cities,30*12);
        }

        return $cities;
    }
}
