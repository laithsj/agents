<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripCity extends Model
{
    public function city(){
		return $this->belongsTo('App\Cities');
    }

    public function addTripCity($tripId,$cityId){
        $this->trip_id = $tripId;
        $this->city_id = $cityId;
        $this->save();
    }

    public static function removeTripCities($tripId){
        self::where('trip_id',$tripId)->delete();
    }
}
