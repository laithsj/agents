<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Hotel;
use App\Country;
use App\Api\Booking;
use Cache;
use App\BookingCity;

class UpdateHotelsList extends Command
{
    protected $signature = 'hotels:updateList';

    protected $description = "Update hotels list based on the country ISO";

    public function __construct()
    {
        parent::__construct();
    }
    public function handle(){
        // get countries Codes
        $countries = Country::select('id','code')->get();
        foreach($countries as $country){
            // check if countries has cities in the booking cities
            $hasCities = BookingCity::getCitiesCountByCountryId($country->id);

            // get booking.com countries
            if($hasCities == 0){
                // get hotels list form the api
                $booking = new Booking;
                $bookingAPI = $booking->bookingAPI('getBookingCites',strtolower($country->code));
                if(!isset($bookingAPI['errors'])){
                    $cities = $bookingAPI['result'];
                    if(count($cities) > 0 && !isset($cities['errors'])){
                        for ($i=0; $i < count($cities); $i++) {
                            // prepare city data
                            $cityName = $cities[$i]['name'];
                            $cityId = $cities[$i]['city_id'];
                            $cityHotelsCount = $cities[$i]['nr_hotels'];

                            // add booking cities per country
                            $bookingCity = new BookingCity;
                            $addBookingCity = $bookingCity->addCountryCity($country->id,$cityId,$cityName,$cityHotelsCount);
                        }

                    }
                }

            }

                // for ($i=0; $i == count($hotelsApi['result']); $i++) {
                //     // $hotelId = $hotelsApi['result'][$p]['hotel_id'];

                //     // Cache::forget('hotel_'.$hotelId);

                //     // // check if the hotel is exist in the database
                //     // $hotel = Hotel::getHotelById($hotelId);
                //     // if(count($hotel) == 0){
                //     //     // add new hotel data
                //     //     $hotelModel = new Hotel;
                //     //     $addHotel = $hotelModel->addNewHotel(
                //     //         $hotelsApi['result'][$p]['hotel_id'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['default_language'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['address'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['hotel_type_id'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['url'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['city_id'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['review_score'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['deep_link_url'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['currency'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['exact_class'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['is_closed'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['ranking'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['number_of_reviews'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['zip'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['city'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['class'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['name'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['number_of_rooms'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['country'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['preferred'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['location']['longitude'],
                //     //         $hotelsApi['result'][$p]['hotel_data']['location']['latitude']
                //     //     );

                //     //     Cache::put('hotel_'.$hotelsApi['result'][$p]['hotel_id'],$addHotel,30*30);
                //     // }
                // }

        }
    }
}
