<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/home',['uses' => 'HomeController@index','middleware' => ['auth']]);
Route::get('/',['uses' => 'HomeController@index','middleware' => ['auth']]);

// login
Route::get('/login',['uses' => 'Auth\LoginController@viewLogin']);
Route::post('/login',['uses' => 'Auth\LoginController@login']);

//logout 
Route::get('/logout',['uses' => 'Auth\LoginController@logout']);
Route::post('/logout',['uses' => 'Auth\LoginController@logout']);


// register
Route::get('/register',['uses' => 'RegistrationController@registerView']);
Route::post('/register',['uses' => 'RegistrationController@register']);
//forgetpassword
Route::get('/forgetPassword',['uses' =>'ForgotPasswordController@ForgotPasswordView']);
Route::post('/forgetPassword',['uses' =>'ForgotPasswordController@ForgotPassword']);


// trips routes
Route::get('/trip-list',['uses' => 'TripController@viewTripsList','middleware' => ['auth']]);
Route::get('/trip-create-internal',['uses' => 'TripController@viewCreateInternalTrip','middleware' => ['auth']]);
Route::post('/get-country-cities',['uses' => 'CitiesController@getCityByCountry','middleware' => ['auth']]);
Route::post('/trip-create-internal',['uses' => 'TripController@createTripInternal','middleware' => ['auth']]);
Route::get('/view-trip',['uses' => 'TripController@viewTripDetails','middleware' => ['auth']]);
Route::get('/trip-files-download',['uses' => 'TripController@downloadTripFiles','middleware' => ['auth']]);
Route::get('/edit-trip',['uses' => 'TripController@viewEditTrip','middleware' => ['auth']]);
Route::post('/edit-trip',['uses' => 'TripController@editTrip','middleware' => ['auth']]);
Route::get('/delete-trip',['uses' => 'TripController@deleteTrip','middleware' => ['auth']]);
Route::get('/trip-create-external',['uses' => 'TripController@viewTripExternal','middleware' => ['auth']]);
Route::post('/get-country-hotels',['uses' => 'HotelController@getHotelsByCountry','middleware' => ['auth']]);
Route::post('/trip-create-external',['uses' => 'TripController@createTripExternal','middleware' => ['auth']]);


// profile routes
Route::get('/profile',['uses' => 'ProfileController@viewPorfile','middleware' => ['auth']]);
Route::get('/profile-edit',['uses' => 'ProfileController@viewEditProfile','middleware' => ['auth']]);
Route::post('/profile-update-emails',['uses' => 'ProfileController@editProfileEmails','middleware' => ['auth']]);


// Route::get('/home', 'HomeController@index')->name('home');
