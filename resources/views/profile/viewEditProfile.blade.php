<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Edit Profile</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/main.min.css">
    <link rel="stylesheet" href="css/skins/skin-purple-light.min.css ">
    <meta name="csrf-token" content="qPRTP521Gu2cg26qSvNwCEP51IvsiLTFcblwXKS4" />
    <link rel="stylesheet" href="css/edit_profile.css ">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-purple-light sidebar-mini layout-top-nav">
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">
            <nav class="navbar navbar-static-top" style="background-color:#809fff!important;">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{action('ProfileController@viewPorfile')}}" class="navbar-brand">
                            <b>happy travels</b>
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-fw fa-plane "></i>
                                    Trip
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="active ">
                                        <a href="{{action('TripController@viewTripsList')}}" class="list">
                                            <i class="fa fa-list-alt "></i>
                                            Trips List
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{action('TripController@viewCreateInternalTrip')}}"  class="list">
                                            <i class="fa fa-pencil-square "></i>
                                            Create Domestic Trip
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{action('TripController@viewTripExternal')}}"  class="list">
                                            <i class="fa fa-pencil-square "></i>
                                            Create International Trip
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="#">
                                <a href="{{action('ProfileController@viewPorfile')}}">
                                    <i class="fa fa-fw fa-user "></i>
                                    Profile
                                </a>
                            </li>
                            <li class="">
                                <a href="{{action('ProfileController@viewEditProfile')}}">
                                    <i class="fa fa-fw fa-edit "></i>
                                    Edit Profile
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="{{action('Auth\LoginController@logout')}}">
                                    <i class="fa fa-fw fa-power-off"></i> Log Out
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="content-wrapper">
            <div class="container">
                <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if(trim($message) == 'not updated')
                    <div class="alert alert-danger alert-dismissible" id="failMessage">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>
                            <i class="icon fa fa-ban"></i>
                            Alert!
                        </h4>
                        Something went wrong while adding the new updates
                    </div>
                @elseif(trim($message) == 'updated')
                    <div class="callout callout-success" id="successMessage">
                        <h4>Success!</h4>
                       <p>Your new update occured succesfuly.</p>
                    </div>
                @endif
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li><a href="#editDetails" data-toggle="tab">Edit Profile Details</a></li>
                        <li><a href="#emailEdit" data-toggle="tab">Edit Email Details</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane " id="emailEdit">
                        <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="inputName" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Password</label>

                                    <div class="col-sm-10">
                                        <input type="Password" class="form-control" id="inputName" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                               <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-danger">Update</button>
                                            </div>
                                        </div>
                            </form>
                        </div>
                        <div class="active tab-pane " id="editDetails">
                        </div>
                    </div>
                </div>
    </section>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="js/jquery.min.js"></script>   
     <script src="js/bootstrap.min.js"></script>
    </body>
    </html>