<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Log In</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="css/main2.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="css/skins/skin-purple-light.min.css ">
        <meta name="csrf-token" content="qPRTP521Gu2cg26qSvNwCEP51IvsiLTFcblwXKS4" />
        <link rel="stylesheet" href="css/log_in.css ">

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="login-page">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <p class="login_logo">Agent </p>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <div class="col-lg-5 col-7  login-box">
                            <p class="">Sign In</p>
                            <form action="{{action('Auth\LoginController@login')}}" method="post" >
                                {{ csrf_field() }}
                                <div class="form-group ">
                                    <input type="email" class="form-control" placeholder="Email" required name="email">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" placeholder="Password" required name="password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                            @if($errors->any())
                            <div class="row">
                                <div class="col-lg-12 mb-3 d-flex justify-content-center">
                                    <div class="card bg-danger text-white shadow">
                                        <div class="card-body "style="padding:5px 17px!important;">
                                            Opps!
                                        <div class="text-white small">
                                        {{$errors->first()}}
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                                <div class="row">
                                    <div class="col-lg-4 col-6">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat sign-in">
                                            Sign In
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="col-12 sign_in_data">
                                <a href="{{action('ForgotPasswordController@ForgotPasswordView')}}">I forgot my password</a><br>
                                <a href="{{action('RegistrationController@registerView')}}" class="text-center">Create  New Acount</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
