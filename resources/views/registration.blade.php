<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Registration</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="css/main2.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="css/skins/skin-purple-light.min.css ">
        <meta name="csrf-token" content="qPRTP521Gu2cg26qSvNwCEP51IvsiLTFcblwXKS4" />
        <link rel="stylesheet" href="css/regestration_agent.css ">
        <!-- Google Font -->
        <link rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="login-page">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <p class="login_logo">Regestration</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row ">
                <div class="col-12 d-flex justify-content-center">
                    <div class="col-lg-5 col-7  login-box mb-5">
                        <p class="">Register a new membership</p>
                        <form action="{{action('RegistrationController@register')}}"id="validatedForm" method="post" enctype="multipart/form-data"
                      >
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" class="form-control" name="name"  pattern="[a-zA-Z ]*" oninput="setCustomValidity('')" oninvalid="setCustomValidity('Please enter on alphabets only. ')" id="name" required placeholder="Agent name">
                                <span class="glyphicon glyphicon-user "></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="email" class="form-control" name="email" required placeholder="Email">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" class="form-control" name="password" required placeholder="Password" id="Password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                               >
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" class="form-control" id="confirmPassword" required placeholder="Retype password"
                             >
                                <span class="glyphicon glyphicon-log-in form-control-feedback" ></span>
                           <div class="btn btn-danger mt-3"id="NotValid"style="display:none;width: 50%;
                                   font-size: 14px;
                                  margin-left: 17px;"></div>
                            </div>
                            <div class="row">
                                    <div class="col-12">
                                       <div class="checkbox icheck">
                                           <label>
                                            <input type="checkbox" required class="mt-3"> I agree to the <a href="#">terms</a>
                                            </label>
                                        </div>
                                    <div class="mt-3 mb-2">
                                            <label for="">please enter your Commercial document
                                            </label>
                                            <input type="file" name="myFile[]" required accept="file" multiple aria-label=" choose Commercial document for your company " >
                                        </div>
                                </div>
                          </div>
                                <!-- /.col -->
                                <div class="row">
                                   <div class="col-lg-12 mb-3 mt-3 d-flex justify-content-center">
                                      <button type="submit" id="register"class="btn btn-primary btn-block btn-flat">Register</button>
                                    </div>
                                </div>
                                <!-- /.col -->
                                 @if(isset($message) && $message != '')
                                <div class="row">
                                   <div class="col-lg-12 d-flex justify-content-center">
                                     <p class="btn btn-danger"> {{$message}} !</p>
                                   <div>
                                </div>
                                </div>
                                @endif
                               

                              <div class="row">
                                   <div class="col-lg-12 mb-3  ">
                            <div class="mb-4 ">
                                <a href="{{action('Auth\LoginController@viewLogin')}}" class="text-center">I already have an account</a>
                            </div>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="{{env('http:ent.happytravel.com//ag')}}/js/bootstrap.min.js"></script>
        <script src="{{env('http:ent.happytravel.com//ag')}}/js/validation.js"></script>

    </body>
</html>
