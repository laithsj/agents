<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Trips List</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/main.min.css">
    <link rel="stylesheet" href="css/skins/skin-purple-light.min.css ">
    <meta name="csrf-token" content="qPRTP521Gu2cg26qSvNwCEP51IvsiLTFcblwXKS4" />
    <link rel="stylesheet" href="css/triplis.css ">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-purple-light sidebar-mini layout-top-nav">
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">
            <nav class="navbar navbar-static-top"style="background-color:#809fff!important;">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{action('ProfileController@viewPorfile')}}" class="navbar-brand">
                            <b>happy travels</b>
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-fw fa-plane "></i>
                                    Trip
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="active ">
                                        <a href="{{action('TripController@viewTripsList')}}" class="list">
                                            <i class="fa fa-list-alt "></i>
                                            Trips List
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{action('TripController@viewCreateInternalTrip')}}"  class="list">
                                            <i class="fa fa-pencil-square "></i>
                                            Create Domestic Trip
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{action('TripController@viewTripExternal')}}"  class="list">
                                            <i class="fa fa-pencil-square "></i>
                                            Create International Trip
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="#">
                                <a href="{{action('ProfileController@viewPorfile')}}">
                                    <i class="fa fa-fw fa-user "></i>
                                    Profile
                                </a>
                            </li>
                            <li class="">
                                <a href="{{action('ProfileController@viewEditProfile')}}">
                                    <i class="fa fa-fw fa-edit "></i>
                                    Edit Profile
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="{{action('Auth\LoginController@logout')}}">
                                    <i class="fa fa-fw fa-power-off"></i> Log Out
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="content-wrapper">
            <div class="container">
                <!-- Main content -->
                @if(trim($messageStatus) != '' && $messageStatus == 'success')
    <div class="callout callout-success" id="successMessage">
        <h4>Success Adding A Trip!</h4>
        <p>A new trip has been added successfuly to the list.</p>
    </div>
@elseif(trim($messageStatus) != '' && $messageStatus == 'fail')
    <div class="alert alert-danger alert-dismissible" id="failMessage">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4>
            <i class="icon fa fa-ban"></i>
            Alert!
        </h4>
        {{$message}}
    </div>
@endif
<div class="box box-default">
    <div class="box-header with-border">
            <h3 class="box-title">Your Trips List</h3>
        </div>
    <div class="box-body">
        @if(count($trips) > 0)
        <table id="" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Trip Country</th>
                    <th>Trip Start From</th>
                    <th>Trip Ends In</th>
                    <th>Trip Age</th>
                    <th>Trip Price Per Person</th>
                    <th>Trip Extra Price Per Person</th>
                    <th>Trip Total Price Per Person</th>
                    <th>Trip Edit</th>
                    <th>Trip View</th>
                </tr>
            </thead>
            <tbody>
                @foreach($trips as $trip)
                    <tr>
                        <td>
                            @if($trip->country_id > 0)
                                {{$trip->country->name}}
                            @else
                                International Trip
                            @endif
                        </td>
                        <td>{{date_format(date_create($trip->trip_start_date),'Y-m-d')}}</td>
                        <td>{{date_format(date_create($trip->trip_end_date),'Y-m-d')}}</td>
                        <td>
                            @if($trip->age == 0)
                                Both Adult & Kids
                            @elseif($trip->age == 1)
                                Only Adult
                            @else
                                Only Kids
                            @endif
                        </td>
                        <td>{{$trip->currency}} {{$trip->trip_price_per_person}}</td>
                        <td>{{$trip->currency}} {{$trip->trip_extra_price}}</td>
                        <td>{{$trip->currency}} {{$trip->trip_total_price}}</td>
                        <td>
                            <a href="/edit-trip?id={{$trip->id}}&enc={{md5($trip->id.Auth::user()->id)}}" class="btn btn-block btn-info btn-xs">Edit Trip</a>
                        </td>
                        <td>
                            <a href="/view-trip?id={{$trip->id}}&enc={{md5($trip->id.Auth::user()->id)}}" class="btn btn-block btn-info btn-xs">View Trip</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
            <h1 class="text-center"> No Trips Are Registered In Your Account </h1>
        @endif
    </div>
</div>
</section>
    <div class="control-sidebar-bg"></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="js/jquery.min.js"></script>   
     <script src="js/bootstrap.min.js"></script>
    </body>
    </html>