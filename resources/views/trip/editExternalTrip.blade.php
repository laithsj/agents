<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Edit External Trips</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/main.min.css">
    <link rel="stylesheet" href="css/skins/skin-purple-light.min.css ">
    <meta name="csrf-token" content="qPRTP521Gu2cg26qSvNwCEP51IvsiLTFcblwXKS4" />
    <link rel="stylesheet" href="css/EditTrip.css ">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-purple-light sidebar-mini layout-top-nav">
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">
            <nav class="navbar navbar-static-top"style="background-color:#809fff!important;">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{action('ProfileController@viewPorfile')}}" class="navbar-brand">
                            <b>happy travels</b>
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-fw fa-plane "></i>
                                    Trip
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="active ">
                                        <a href="{{action('TripController@viewTripsList')}}" class="list">
                                            <i class="fa fa-list-alt "></i>
                                            Trips List
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{action('TripController@viewCreateInternalTrip')}}"  class="list">
                                            <i class="fa fa-pencil-square "></i>
                                            Create Domestic Trip
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="{{action('TripController@viewTripExternal')}}"  class="list">
                                            <i class="fa fa-pencil-square "></i>
                                            Create International Trip
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="#">
                                <a href="{{action('ProfileController@viewPorfile')}}">
                                    <i class="fa fa-fw fa-user "></i>
                                    Profile
                                </a>
                            </li>
                            <li class="">
                                <a href="{{action('ProfileController@viewEditProfile')}}">
                                    <i class="fa fa-fw fa-edit "></i>
                                    Edit Profile
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="{{action('Auth\LoginController@logout')}}">
                                    <i class="fa fa-fw fa-power-off"></i> Log Out
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="content-wrapper">
            <div class="container">
                <!-- Main content -->
@if(trim($messageStatus) != '' && $messageStatus == 'fail')
    <div class="alert alert-danger alert-dismissible" id="failMessage">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4>
            <i class="icon fa fa-ban"></i>
            Alert!
        </h4>
        {{$message}}
    </div>
@endif
<form  action="/edit-trip" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="tripId" value="{{$trip->id}}">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Trip Country | Date | Age | Price</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="overlay hide spinOverlay" id="spinOverlay">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <div class="box-body">
            {{ csrf_field() }}
            <div class="row" id="tripCreateRow">
                <div class="col-md-6" id="countriesSelect">
                    <div class="form-group">
                        <label>Trip Start Direction Country</label>
                        <select class="form-control select2" name="countrySelected" id="tripCountryExternalFrom">
                            {{-- <option selected="selected" value="">Please Pick A Country</option> --}}
                            @foreach($countries as $country)
                                <option value="{{$country->code}}" @if($trip->from_country == $country->code) selected @endif>{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6" id="countriesSelect">
                    <div class="form-group">
                        <label>Trip End Direction Country</label>
                        <select class="form-control select2" name="countrySelected2" id="tripCountryExternalTo">
                            <option selected="selected" value="">Please Pick A Country</option>
                            @foreach($countries as $country)
                                <option value="{{$country->code}}" @if($trip->to_country == $country->code) selected @endif>{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="citiesInputFeild">
                    <div class="form-group">
                        <label>Trip Hotels List</label>
                        <select class="form-control select2" name="selectedHotels[]" multiple="multiple" data-placeholder="Select Trip Hotels List" style="width: 100%;" id="countryCitiesExternal">
                            {{-- @if(count($hotels) > 0) --}}
                                @foreach($hotels as $hotel)
                                    <option value="{{$hotel->hotel->hotel_id}}" selected>{{$hotel->hotel->name}}</option>
                                @endforeach
                            {{-- @endif --}}
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Trip Date:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="date" value="{{date_format(date_create($trip->trip_start_date),'m/d/Y')}} - {{date_format(date_create($trip->trip_end_date),'m/d/Y')}}" class="form-control pull-right" id="tripDate">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Age Restriction</label>
                        <select class="form-control select2"  name="age" style="width: 100%;">
                            <option value="1" @if($trip->age == 1) selected @endif>Both</option>
                            <option value="2" @if($trip->age == 2) selected @endif>Adults</option>
                            <option value="3" @if($trip->age == 3) selected @endif>Kids</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-3" id="">
                    <div class="form-group">
                        <label>Price Per Person</label>
                    <input type="text" class="form-control" id="tripPrice" name="tripPrice" value="{{$trip->trip_price_per_person}}" placeholder="Enter Price">
                    </div>
                </div>
                <div class="col-md-3" id="">
                    <div class="form-group">
                        <label>Extra Price</label>
                        <input type="text" class="form-control" id="extraPrice" name="extraPrice" value="{{$trip->trip_extra_price}}" placeholder="Enter Extra Price">
                    </div>
                </div>
                <div class="col-md-3" id="">
                    <div class="form-group">
                        <label>Price Currency</label>
                        <select class="form-control select2" name="currency">
                            <option value="JOD" @if($trip->currency == 'JOD') selected @endif>JOD</option>
                            <option value="USD" @if($trip->currency == 'USD') selected @endif>USD</option>
                            <option value="EURO" @if($trip->currency == 'EURO') selected @endif>EURO</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3" id="">
                    <div class="form-group">
                        <label>Total Trip Price</label>
                        <input type="text" class="form-control" id="totalPrice" name="totalPrice" value="{{$trip->trip_total_price}}" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Trip Images | Files</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="overlay hide spinOverlay" id="">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Upload Trip File | Image</label>
                        <input type="file" name="file[]" multiple class="form-control pull-right" id="file">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Trip Additional Information</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <div class="overlay hide spinOverlay" id="">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Extra Information</label>
                        <textarea name="content" id="editorCreateTrip">{{$trip->extra_details}}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" id="createTrip" class="btn btn-primary">Update Trip</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</section>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/GlobalTrip.js"></script>
</body>
</html>


